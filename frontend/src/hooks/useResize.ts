import { MutableRefObject, RefObject, useCallback, useEffect, useRef, useState } from 'react';

interface IState {
    screenWidth: number;
    screenHeight: number;
    rect?: DOMRect;
}

export const useResize = <T extends HTMLElement>(element?: MutableRefObject<T> | RefObject<T>): IState => {
    const [state, setState] = useState<IState>({
        screenWidth: 0,
        screenHeight: 0,
        rect: undefined,
    });

    const onResize = useCallback(() => {
        const screenWidth = window.innerWidth;
        const screenHeight = window.innerHeight;
        const rect = element?.current?.getBoundingClientRect();

        setState({
            screenWidth,
            screenHeight,
            rect,
        });
    }, [element?.current]);

    const resizeObserver = useRef(new ResizeObserver(() => {
        onResize();
    }));

    useEffect(() => {
        onResize();
        window.addEventListener('resize', onResize, false);
        return () => {
            window.removeEventListener('resize', onResize, false);
        };
    }, [onResize]);

    useEffect(() => {
        if (element?.current) {
            // @ts-ignore
            resizeObserver.current.observe(element.current);
        }
        return () => {
            if (element?.current) {
                // @ts-ignore
                resizeObserver.current.unobserve(element.current);
            }
        };
    }, [element?.current]);

    useEffect(() => () => {
        resizeObserver.current.disconnect();
        window.removeEventListener('resize', onResize, false);
    }, []);

    return state;
}
