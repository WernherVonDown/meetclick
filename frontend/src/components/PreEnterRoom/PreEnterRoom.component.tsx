import React, { useCallback, useContext, useState } from "react";
import { CenterXY } from "../../common/CenterXY/CenterXY";
import { MediaSocketContextProvider } from "../../context/MediaSocketContext";
import { RoomContextProvider } from "../../context/RoomContext";
import { StreamContext } from "../../context/StreamContext";
import { VideoChatContextProvider } from "../../context/VideoChatContext";
import { PreEnterRoomForm } from "../EnterRoomForm/EnterRoomForm.component";
import { MainLayout } from "../MainLayout/MainLayout";
import { Room } from "../Room/Room.component";
import { RoomLayout } from "../RoomLayout/RoomLayout.component";
import { StreamSettings } from "../StreamSettings/StreamSettings.component";

interface IProps {
    roomId: string;
}

export const PreEnterRoom: React.FC<IProps> = ({ roomId }) => {
    const { state: { devicesSet } } = useContext(StreamContext);
    const [userNameSet, setUserNameSet] = useState(false);
    const [userName, setUserName] = useState('')

    const onUserNameSet = useCallback((uName: string) => {
        setUserName(uName);
        setUserNameSet(true);
    }, [])

    if (!devicesSet) {
        return (
            <MainLayout>
                <CenterXY>
                    <StreamSettings />
                </CenterXY>
            </MainLayout>
        )
    }

    if (!userNameSet || !userName) {
        return (
            <MainLayout>
                <CenterXY>
                    <PreEnterRoomForm done={onUserNameSet} />
                </CenterXY>
            </MainLayout>
        )
    }

    return (
        <MediaSocketContextProvider roomId={roomId} userName={userName}>
            <RoomContextProvider roomId={roomId} userName={userName}>
                <VideoChatContextProvider>
                    <RoomLayout>
                        <Room />
                    </RoomLayout>
                </VideoChatContextProvider>
            </RoomContextProvider>
        </MediaSocketContextProvider>
    )
}
