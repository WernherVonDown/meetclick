import React, { useContext } from "react";
import { RoomContext } from "../../context/RoomContext";
// import { TextChatWidget } from "../TextChatWidget/TextChatWidget.component";
import { VideoChat } from "../VideoChat/VideoChat.component";
import styles from "./Room.module.scss";

import dynamic from 'next/dynamic'
import { Suspense } from 'react'
//@ts-ignore https://nextjs.org/docs/advanced-features/dynamic-import
const TextChatWidget = dynamic(() => import('../TextChatWidget/TextChatWidget.component').then((mod) => mod.TextChatWidget), {ssr: false});

interface IProps {

}

export const Room: React.FC<IProps> = () => {
    const { state: { userName } } = useContext(RoomContext);
    return (
        <div className={styles.roomWrapper}>
            <VideoChat />
            {/* <TextChatWidget /> */}
            <Suspense fallback={`Loading...`}>
      <TextChatWidget />
    </Suspense>
        </div>
    )
}
