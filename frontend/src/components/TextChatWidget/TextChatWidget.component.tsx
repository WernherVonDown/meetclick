import React, { useCallback, useContext, useEffect } from 'react';
import { Widget, addResponseMessage } from 'react-chat-widget';


import 'react-chat-widget/lib/styles.css';
import { TextChatEvents } from '../../const/textChat/TextChatEvents';
import { MediaSocketContext } from '../../context/MediaSocketContext';
import { useMediaSocketSubscribe } from '../../hooks/useMediaSocketSubscribe';


export const TextChatWidget: React.FC = () => {
    const { actions: { emit } } = useContext(MediaSocketContext);

    const onNewTextChatMessage = useCallback(({ message, userName }: { message: string, userName: string }) => {
        console.log("onNewTextChatMessage", {message, userName})
        addResponseMessage(message, userName);
    }, []);

    const handleNewUserMessage = useCallback((e: any) => {
        console.log('handleNewUserMessage', e)
        emit(TextChatEvents.textChatMessage, { message: e })
    }, []);

    useMediaSocketSubscribe(TextChatEvents.textChatMessage, onNewTextChatMessage)

    return (
        <Widget
            title=""
            subtitle=""
            emojis={true}
            handleNewUserMessage={handleNewUserMessage}
        />
    )
}