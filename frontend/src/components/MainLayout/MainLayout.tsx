import React, { ReactNode, useContext } from "react";
import { AppBar, Box, Button, Container, styled, Toolbar, Typography } from "@mui/material";
import { LoginRoute, RegistrationRoute } from "../../const/API_ROUTES";
import Link from "next/link";
import { AuthContext } from "../../context/AuthContext";

const Offset = styled('div')(({ theme }) => theme.mixins.toolbar);

interface IProps {
    children: ReactNode | ReactNode[]
}

export const MainLayout: React.FC<IProps> = ({ children }) => {
    const { state: { loggedIn, user }, actions: { logout } } = useContext(AuthContext);
    return (
        <>
            <AppBar position="fixed">
                <Toolbar>
                    <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                        <Link href="/">MeetClick</Link>
                    </Typography>
                    {!loggedIn ? <>
                        <Button href={RegistrationRoute} color="inherit" variant='text'>
                            <Typography>Регистрация</Typography> 
                        </Button>
                        <Button href={LoginRoute} sx={{px: '30px'}} color="inherit" variant='outlined'>
                        <Typography>Вход</Typography> 
                        </Button>
                    </> : <>
                        <Button onClick={logout} color="inherit" variant='outlined'>
                            Выйти
                        </Button>
                    </>
                    }

                </Toolbar>
            </AppBar>
            {/* <Offset /> */}
            <main>
                {/* <Container fixed> */}
                {children}
                {/* </Container> */}
            </main>
        </>
    )
}