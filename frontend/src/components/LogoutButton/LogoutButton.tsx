import React, { useContext } from "react";
import { AuthContext } from "../../context/AuthContext";

export const LogoutButton: React.FC = () => {
    const { actions: { logout } } = useContext(AuthContext);

    return <button onClick={logout}>Выход</button>
}