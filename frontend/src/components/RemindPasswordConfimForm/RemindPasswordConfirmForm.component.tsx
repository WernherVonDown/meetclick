import { Button, TextField, Typography, Link } from "@mui/material"
import { useRouter } from "next/router";
import { useCallback, useContext, useState, useEffect } from 'react';
import { RemindPasswordRoute, RegistrationRoute, LoginRoute } from '../../const/API_ROUTES';
import { AuthContext } from "../../context/AuthContext";
import { useInput } from "../../hooks/useInput";

export const RemindPasswordConfirmForm = () => {
    const { actions: { resetPasswordConfirm } } = useContext(AuthContext);
    const [resetError, setResetError] = useState(false);
    const [token, setToken] = useState('');
    const [email, setEmail] = useState('');
    const router = useRouter()
    const password1 = useInput('');
    const password2 = useInput('');

    useEffect(() => {
        if (router.isReady) {
            const { token, email } = router.query;
            if (token && email && typeof token === 'string' && typeof email === 'string') {
                setEmail(email);
                setToken(token);
                
            } else {
                setResetError(true);
            }
        }
        
    }, [router.isReady])

    const onResetPassword = useCallback(async () => {
        if (!password1.value.length || !password2.value.length) {
            return alert('Заполните все поля');
        }

        if (password1.value.length !== password2.value.length) {
            return alert('Пароли не совпадают')
        }

        interface IRes {
            success: boolean
        }
        const res = await resetPasswordConfirm(email, token, password1.value) as IRes;
        if (!res.success) {
            setResetError(true)
        } else {
            router.push(LoginRoute);
        }
    }, [password1.value, password2.value, token, email]);

    if (resetError) {
        return (
            <div>
                <Typography>Ошибка восстановления пароля</Typography>
                <Typography variant="caption">
                    <div>Ошибка восстановления, попробуйте еще раз </div>
                    <div><Link href={RemindPasswordRoute}>восстановление пароля</Link></div>
                    <div>
                        <Link href={LoginRoute}>Вход</Link> / <Link href={RegistrationRoute}>Регистрация</Link>
                    </div>
                </Typography>
            </div>
        )
    }

    return (
        <div>
            <Typography variant="h5">Восстановление пароля</Typography>
            <TextField type="password" variant="standard" {...password1} label="Новый пароль" />
            <TextField type="password" variant="standard" {...password2} label="Повторите пароль" />
            <Button variant='contained' onClick={onResetPassword}>
                Отпарвить
            </Button>
        </div>)
}