import { IconButton, MenuItem, Select, SvgIcon, Typography } from "@mui/material";
import React, { useContext } from "react";
import { Video } from "../../common/Video/Video.component";
import styles from "./ConferenceVideo.module.scss";
import MicOffIcon from '@mui/icons-material/MicOff';
import VideocamOffIcon from '@mui/icons-material/VideocamOff';
import { StreamContext } from "../../context/StreamContext";
import { IDevicePermissions } from "../../const/videoChat/types";
import classNames from "classnames";
import { VideoControls } from "../VideoControls/VideoControls.component";


interface IProps {
    stream: MediaStream;
    isLocal?: boolean;
    userName?: string;
    devices: IDevicePermissions;
}

export const ConferenceVideo: React.FC<IProps> = ({ stream, isLocal, userName, devices = { video: true, audio: true } }) => {

    return (
        <div className={styles.conferenceVideoWrapper}>
            {<Video stream={stream} className={classNames({ [styles.mirrored]: isLocal })} muted={isLocal || !devices.audio} />}
            {!devices.video && <div className={styles.disabledCameraPoster}>{isLocal ? <SvgIcon fontSize="large"><VideocamOffIcon /></SvgIcon> : userName}</div>}
            {!devices.audio && !isLocal && <div className={styles.disabledMicIcon}><SvgIcon fontSize="large"><MicOffIcon /></SvgIcon></div>}
            {isLocal && <VideoControls />}
            {!isLocal && <div className={styles.userName}>
                <Typography color={'white'}>{userName}</Typography>
            </div>}
        </div>
    )
}
