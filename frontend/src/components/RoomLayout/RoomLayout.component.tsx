import { AppBar, Box, Button, Container, Link, Toolbar, Typography, styled, Grid, IconButton } from "@mui/material";
import React, { ReactNode, useContext, useMemo } from "react";
import { RoomContext } from "../../context/RoomContext";
import styles from "./RoomLayout.module.scss"

const Offset = styled('div')(({ theme }) => theme.mixins.toolbar);

interface IProps {
    children: ReactNode | ReactNode[];
}

export const RoomLayout: React.FC<IProps> = ({ children }) => {
    const { state: { users } } = useContext(RoomContext);
    const numberOfUsers = useMemo(() => {
        return users.length;
    }, [users.length]);

    return (
        <div className={styles.roomLayout}>
            <Box sx={{ flexGrow: 1 }}>
                <AppBar position="static">
                    <Toolbar>
                        <IconButton
                            size="large"
                            edge="start"
                            color="inherit"
                            aria-label="menu"
                            sx={{ mr: 2 }}
                        >
                            MeetClick
                            {/* <MenuIcon /> */}
                        </IconButton>
                        <Typography component="div" sx={{ flexGrow: 1 }}>
                            Пользователи: {numberOfUsers}
                        </Typography>
                        <Button color="inherit" href="/">Выйти</Button>
                    </Toolbar>
                </AppBar>
            </Box>

            {/* <Container maxWidth={false}> */}
            {/* <Offset /> */}
            <div className={styles.roomContent}>
                {children}
            </div>

            {/* </Container> */}
        </div>
    )
}
