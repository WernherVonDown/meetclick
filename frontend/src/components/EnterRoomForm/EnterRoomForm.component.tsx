import React, { useCallback, useContext } from 'react';
import { Button, Grid, Paper, TextField, Typography } from '@mui/material';
import { useInput } from '../../hooks/useInput';

interface IProps {
    done: (userName: string) => void
}

export const PreEnterRoomForm: React.FC<IProps> = ({ done }) => {
    const userName = useInput('');

    const handleDone = useCallback(() => {
        if (userName.value.trim()) {
            done(userName.value.trim())
        } else {
            alert('Заполните поле')
        }
    }, [userName.value])

    return <Paper><Grid padding={3} container direction={"column"}>
        <Typography variant='h5'>Введите имя</Typography>
        <TextField variant="outlined" margin='normal' fullWidth size='small' {...userName} type={'text'} label='Ваше имя' />
        <Button fullWidth sx={{ marginTop: 1 }} variant='contained' onClick={handleDone}>
            Войти
        </Button>
    </Grid></Paper>;
}