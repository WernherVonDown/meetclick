import React, { ReactElement, useCallback, useEffect, useMemo, useState } from 'react';
import { Qualities } from '../const/webrtc/QUALITIES';
import { getBluredVideo } from '../utils/webrtc/getBluredStream';
import { getConstraints } from '../utils/webrtc/getConstraints';
import { getUserMedia } from '../utils/webrtc/getUserMedia';
import { stopStream } from '../utils/webrtc/stopStream';

interface IState {
    stream: MediaStream;
    devicesSet: boolean;
    devicePermissions: { audio: boolean, video: boolean };
    blurVideo: boolean;
}

interface IProps {
    state: IState,
    children: ReactElement | ReactElement[]
}

interface ContextValue {
    state: IState,
    actions: {
        [key: string]: (...args: any[]) => unknown
    }
}

const StreamContext = React.createContext({} as ContextValue);

const StreamContextProvider = (props: IProps) => {
    const { children, state: defaultState } = props;
    const [stream, setStream] = useState<MediaStream>(defaultState.stream);
    const [devicesSet, setDevicesSet] = useState(false);
    const [devicePermissions, setDevicePermissions] = useState({ audio: true, video: true });
    const [blurVideo, setBlurVideo] = useState(false);
    const [videoBlured, setVideoBlured] = useState(false);

    const enableBlur = useCallback(async () => {
        setVideoBlured(true)
        const bluredStream = await getBluredVideo(stream.clone());
        setStream(bluredStream)
    }, [stream])

    useEffect(() => {
        if (stream && blurVideo && !videoBlured) {
            enableBlur();
        }
    },[stream, blurVideo, videoBlured]);

    const disableBlur = useCallback(async () => {
        setVideoBlured(false)
        getStream()
    }, [stream])

    useEffect(() => {
        if (stream && !blurVideo && videoBlured) {
            disableBlur();
        }
    },[stream, blurVideo, videoBlured])

    const getStream = useCallback(async () => {
        try {
            const constraints = getConstraints({
                audio: true,
                video: true,
                // videoDeviceId: videoDeviceId,
                // audioDeviceId: audioDeviceId,
                quality: Qualities["720p"]
            });
            const stream = await getUserMedia(constraints);
            setStream(stream);
        } catch (error) {
            console.log('getStream, error;', error);
        }

    }, []);

    useEffect(() => {
        getStream();
    }, []);

    useEffect(() => {
        if (stream) {
            stream.getVideoTracks().map(t => t.enabled = devicePermissions.video)
        }
    }, [devicePermissions.video, stream]);

    useEffect(() => {
        if (stream) {
            stream.getAudioTracks().map(t => t.enabled = devicePermissions.audio)
        }
    }, [devicePermissions.audio, stream]);

    useEffect(() => {
        return () => {
            if (stream) {
                stopStream(stream);
            }
        }
    }, [stream])

    const state = useMemo(() => ({
        stream,
        devicesSet,
        devicePermissions,
        blurVideo,
    }), [
        stream,
        devicesSet,
        devicePermissions,
        blurVideo,
    ]);

    const actions = {
        setDevicesSet,
        setDevicePermissions,
        setBlurVideo,
    }


    return <StreamContext.Provider
        value={{
            state,
            actions
        }}
    >
        {children}
    </StreamContext.Provider>
}

StreamContextProvider.defaultProps = {
    state: {
        stream: null
    }
}


export { StreamContext, StreamContextProvider };
