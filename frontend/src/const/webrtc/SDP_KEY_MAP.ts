export const SDP_KEY_MAP = {
    'publish:true_initial:true': 'offer',
    'publish:true_initial:false': 'answer',
    'publish:false_initial:true': 'offer',
    'publish:false_initial:false': 'answer',
};