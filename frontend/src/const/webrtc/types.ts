import { STREAM_TYPE } from "./STREAM_TYPE";

export interface IWebRtcConnection {
    type: string;
    connectionId: string;
    userId: string;
    iceServers?: RTCIceServer[];
    onGotOffer: (args: { sdp?: string; connectionId: string }) => void;
    onGotStream: (args: { stream: MediaStream }) => void;
    onGotCandidate: (args: {
        connectionId: string;
        candidate: RTCIceCandidate;
    }) => void;
    peerConnection?: RTCPeerConnection;
    stream?: MediaStream | null;
    onIceConnectionStateFailed: () => void;
    onIceConnectionStateDisconnected: () => void;
    onDisconnected?: () => void;
    devicePermissions: {audio: boolean, video: boolean};
    streamType: STREAM_TYPE;
    devices?: MediaDevices;
    isInitial?: boolean;
}
