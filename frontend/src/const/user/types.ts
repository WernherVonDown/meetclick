import { IDevicePermissions } from "../videoChat/types";

export interface IUser {
    email: string;
    isActivated: boolean;
    id: string;
}

export interface IRoomUser {
    userName: string;
    socketId: string;
    devices: IDevicePermissions;
}