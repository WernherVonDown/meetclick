export enum VideoChatEvents {
    webrtc = 'videoChat:webrtc',
    devicePermissions = 'videoChat:devicePermissions',
}
