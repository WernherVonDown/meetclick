export const stopStream = (stream: MediaStream) => stream.getTracks().forEach(t => t.stop())
