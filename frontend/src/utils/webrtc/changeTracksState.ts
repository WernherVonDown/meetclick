export const changeTracksState = ({ enabled, tracks }: { enabled: boolean, tracks: MediaStreamTrack[]}) => {
    tracks?.map((t: MediaStreamTrack) => {
      t.enabled = enabled;
    });
  };
  