import { Qualities } from '../../const/webrtc/QUALITIES';

const VIDEO_ASPECT_RATIO = 16 / 9;

const qualities = {
    [Qualities['1080p']]: 1080,
    [Qualities['720p']]: 720,
    [Qualities['480p']]: 480,
    [Qualities['360p']]: 360,
    [Qualities['240p']]: 240,
    [Qualities['180p']]: 180,
    [Qualities['144p']]: 144,
};


export interface Constraints {
    audio: boolean | {
        deviceId: string,
    };
    video?: boolean | {
        aspectRatio?: number,
        deviceId?: string,
        height?: number | {
            max: number,
        },
        width?: number | {
            max: number,
        },
    };
}

export function getConstraints({
    audio,
    video,
    audioDeviceId,
    videoDeviceId,
    quality = Qualities['480p'],
}: {
    audio: boolean,
    video: boolean,
    audioDeviceId?: string,
    videoDeviceId?: string,
    quality?: Qualities,
}): Constraints[] {
    const audioConstraint = (audio && audioDeviceId)
        ? { deviceId: audioDeviceId }
        : audio;

    if (!video) {
        return [
            { audio: audioConstraint },
            { audio },
        ];
    }
    const height = qualities[quality];

    return [
        {
            video: {
                aspectRatio: VIDEO_ASPECT_RATIO,
                deviceId: videoDeviceId,
                height,
            },
            audio: audioConstraint,
        },
        {
            video: {
                aspectRatio: VIDEO_ASPECT_RATIO,
                deviceId: videoDeviceId,
                height: { max: height },
            },
            audio: audioConstraint,
        },
        {
            video: {
                aspectRatio: VIDEO_ASPECT_RATIO,
                deviceId: videoDeviceId,
            },
            audio: audioConstraint,
        },
        {
            video: true,
            audio: audioConstraint,
        },
        {
            video: true,
            audio: true,
        },
        {
            video: {
                aspectRatio: VIDEO_ASPECT_RATIO,
                deviceId: videoDeviceId,
                height,
            },
            audio: false,
        },
        {
            video: {
                aspectRatio: VIDEO_ASPECT_RATIO,
                deviceId: videoDeviceId,
                height: { max: height },
            },
            audio: false,
        },
        {
            video: {
                aspectRatio: VIDEO_ASPECT_RATIO,
                deviceId: videoDeviceId,
            },
            audio: false,
        },
        {
            video: true,
            audio: false,
        },
        {
            video: false,
            audio: audioConstraint,
        },
        {
            video: false,
            audio: true,
        },
    ];
}

export function getScreenConstraints({
    audio = false,
    quality = Qualities['1080p'],
}: {
    audio?: boolean,
    quality?: Qualities,
} = {}): Constraints[] {
    const height = qualities[quality];

    return [
        {
            video: {
                aspectRatio: VIDEO_ASPECT_RATIO,
                height,
            },
            audio,
        },
        {
            video: {
                aspectRatio: VIDEO_ASPECT_RATIO,
                height: { max: height },
            },
            audio,
        },
        {
            video: {
                aspectRatio: VIDEO_ASPECT_RATIO,
            },
            audio,
        },
        {
            video: true,
            audio,
        },
        {
            video: {
                aspectRatio: VIDEO_ASPECT_RATIO,
                height,
            },
            audio: false,
        },
        {
            video: {
                aspectRatio: VIDEO_ASPECT_RATIO,
                height: { max: height },
            },
            audio: false,
        },
        {
            video: {
                aspectRatio: VIDEO_ASPECT_RATIO,
            },
            audio: false,
        },
        {
            video: true,
            audio: false,
        },
    ];
}
