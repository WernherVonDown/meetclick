import { WebRtcConnection } from './WebRtcConnection';
import { CONNECTION_TYPE } from '../../const/webrtc/CONNECTION_TYPE';
import { STREAM_TYPE } from '../../const/webrtc/STREAM_TYPE';
import { IWebRtcConnection } from '../../const/webrtc/types';

class WebRtcController {
  connections: Record<string, any>;
  constructor() {
    this.connections = {};
  }

  async createConnection(data: IWebRtcConnection) {
    const connection = new WebRtcConnection(data);
    this.connections[connection.connectionId] = connection;
    try {
      await connection.createPeerConnection();
      if (connection.isInitial()) {
        await connection.createOffer();
      }
      if (connection.isPublish() && connection.isVideoChat()) {
        await connection.applyDeviceSettings();
      }
      return connection;
    } catch (e) {
      this.connections[connection.connectionId].release();
      delete this.connections[connection.connectionId];
      throw e;
    }
  }

  async stopConnection({ userId, type, streamType }: { userId: string, type?: string, streamType?: STREAM_TYPE }) {
    const connection = this.getConnection({
      userId,
      type,
      streamType,
    });
    if (!connection) {
      return {};
    }
    await this.clearConnection(connection);
    return { connectionId: connection.connectionId };
  }

  async clearConnection(connection: WebRtcConnection) {
    await connection.release();
    delete this.connections[connection.connectionId];
  }

  updateDevicePermissions({ userId, devicePermissions }: { userId: string, devicePermissions: {video: boolean, audio: boolean} }) {
    const connection = this.getConnection({
      userId,
      type: CONNECTION_TYPE.PUBLISH,
      streamType: STREAM_TYPE.VIDEO_CHAT,
    });
    if (connection) {
      connection.updateDevicePermissions({ devicePermissions });
    }
  }

  changePublishStream({ stream, streamType = STREAM_TYPE.VIDEO_CHAT }: {stream: MediaStream, streamType?: STREAM_TYPE }) {
    const connections = this.getConnections({
      type: CONNECTION_TYPE.PUBLISH,
      streamType,
    });
    connections.map(c => c.changeStream(stream));
  }

  async addIceCandidate({ connectionId, candidate }: { connectionId: string, candidate: RTCIceCandidate }) {
    const connection = this.connections[connectionId];
    if (!connection) {
      return;
    }
    await connection.addIceCandidate(candidate);
  }

  async processOffer({ connectionId, offer }: { connectionId: string, offer: string }) {
    const connection = this.connections[connectionId];
    if (!connection) {
      return;
    }
    await connection.processOffer(offer);
  }

  async addAnswer({ connectionId, answer }: { connectionId: string, answer: string }) {
    const connection = this.connections[connectionId];
    if (!connection) {
      return;
    }
    await connection.addAnswer(answer);
  }

  getConnection({ userId, type, streamType }: { userId?: string, type?: string, streamType?: STREAM_TYPE }): WebRtcConnection {
    return Object.values(this.connections).find(
      c =>
        (!userId || c.userId === userId) && (!type || c.type === type) && (!streamType || c.streamType === streamType),
    );
  }

  getConnections({ userId, type, streamType }: { userId?: string, type?: string, streamType?: STREAM_TYPE }) {
    return Object.values(this.connections).filter(
      c =>
        (!userId || c.userId === userId) && (!type || c.type === type) && (!streamType || c.streamType === streamType),
    );
  }

  getAllConnections() {
    return Object.values(this.connections).reduce((acc, v) => [...acc, v], []);
  }

  getAllConnectionsStats() {
    return Promise.all(this.getAllConnections().map(async (c: WebRtcConnection) => c.getStats()));
  }

  clear() {
    return Object.values(this.connections).map(c => this.clearConnection(c));
  }
}

export const webRtcController = new WebRtcController();
