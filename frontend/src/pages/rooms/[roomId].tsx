import { NextPage } from "next";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { PreEnterRoom } from "../../components/PreEnterRoom/PreEnterRoom.component";
import { StreamContextProvider } from "../../context/StreamContext";
import RoomService from "../../services/roomService";

const RoomPage: NextPage = () => {
    const [canJoin, setCanJoin] = useState(false);
    const [loading, setLoading] = useState(true);;
    const router = useRouter();
    const { roomId } = router.query;

    useEffect(() => {
        if (roomId && typeof roomId === 'string') {
            RoomService.get(roomId).then(e => {
                setCanJoin(true);
            }).catch(() => {
                setCanJoin(false);
            }).finally(() => {
                setLoading(false);
            });
        } else {
            setCanJoin(false);
            setLoading(false);
        }

    }, [roomId])

    if (loading) {
        return <div>Connection...</div>
    }

    if (!canJoin) {
        return <div>Room not exists</div>
    }

    return (
        <StreamContextProvider>
            <PreEnterRoom roomId={roomId as string} />
        </StreamContextProvider>
    )
}

export default RoomPage;