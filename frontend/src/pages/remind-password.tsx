import { CenterXY } from "../common/CenterXY/CenterXY"
import { MainLayout } from "../components/MainLayout/MainLayout"
import { RemindPasswordForm } from "../components/RemindPasswordForm/RemindPasswordForm.component"

export default function RemindPasswordPage() {
    return (
        <MainLayout>
            <CenterXY>
                <RemindPasswordForm />
            </CenterXY>
        </MainLayout>
    )
}
