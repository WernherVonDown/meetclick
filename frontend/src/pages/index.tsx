import type { NextPage } from 'next'
import Head from 'next/head'
import styles from '../styles/Home.module.css';
import { AuthContext } from '../context/AuthContext';
import { useCallback, useContext, useEffect } from 'react';
import { MainLayout } from '../components/MainLayout/MainLayout';
import { Box, Button, Grid, Link, Typography } from '@mui/material';
import RoomService from '../services/roomService';
import { useRouter } from 'next/router';

const Home: NextPage = () => {
  const { state: { loggedIn }, actions: { checkAuth } } = useContext(AuthContext);
  const router = useRouter()

  useEffect(() => {
    if (localStorage.getItem('token')) {
      checkAuth()
    }
  }, [])

  const enterRoom = useCallback(async () => {
    try {
      const res = await RoomService.create();
      const roomId = res.data.roomId;
      if (roomId) {
        router.push(`/rooms/${roomId}`)
      }
    } catch (error) {
      console.log('Error create room')
    }
  }, [])


  return (
    <div className={styles.container}>
      <Head>
        <title>MeetClick</title>
        <meta name="description" content="Meet click video chat" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <MainLayout>
        <Grid container component="main" sx={{ height: '100vh' }}>
          {/* <CssBaseline /> */}
          <Grid
            container
            width="100%"
            display={"flex"}
            alignItems={"center"}
            justifyContent={"center"}
            direction={"column"}
            item
            xs={12}

            sx={{
              backgroundImage: 'url(/assets/images/landing_image_meetlick.png)',
              backgroundRepeat: 'no-repeat',
              backgroundColor: (t) =>
                t.palette.mode === 'light' ? t.palette.grey[50] : t.palette.grey[900],
              backgroundSize: 'cover',
              backgroundPosition: 'center',
              color: 'white',
            }}
          >
            <Box color={'white'} display="flex" flexDirection={"column"} textAlign="center" alignItems="center" gap={5}>
              <Typography fontSize="40px">Видеозвонки в один клик</Typography>
              <div><Button variant='outlined' sx={{ px: '20px', py: '9px' }} color={'inherit'} onClick={enterRoom}>
                <Typography fontSize="20px">Начать звонок</Typography>
              </Button></div>
            </Box>
            <Box display={"flex"} justifySelf="flex-end" position={"absolute"} textAlign="center" bottom="30px">
              <Typography fontSize="16px" color="#B5B5B5">Проект бесплатный, но вы можете поддержать разработчика <Link target={'_blank'} href="https://my.qiwi.com/Danyl-AW9i68NXJ1" style={{ color: 'inherit', textDecoration: 'underline'}}>здесь</Link> </Typography>
            </Box>
          </Grid>

        </Grid>
      </MainLayout>
    </div>
  )
}

export default Home
