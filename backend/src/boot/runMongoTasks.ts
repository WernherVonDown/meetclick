import { roomService } from "../services/room.service"

export const runMongoTasks = async () => {
    await roomService.clearAllUsersInAllRooms()
}