import mongoose, { Connection, ConnectOptions } from 'mongoose';
import { vars } from '../config/vars';

const { mongo } = vars;

const RECONNECT_TIMEOUT = 15000;

// mongoose.set('useCreateIndex', true);
// mongoose.Promise = Promise;

// if (env === 'development') {
//     mongoose.set('debug', true);
// }

async function startMongo(): Promise<Connection> {
    console.log(`Mongo connected to ${mongo.url}`);
    await mongoose.connect(<string>mongo.url, <ConnectOptions>{
        useNewUrlParser: true,
        useUnifiedTopology: true,
    }
    );
    const { connection } = mongoose;
    return connection;
}

mongoose.connection.on('error', (err) => {
    console.log(`MongoDB connection error: ${err}`);
    setTimeout(startMongo, RECONNECT_TIMEOUT);
});

export default startMongo;
