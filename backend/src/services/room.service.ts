import { v4 } from "uuid";
import { IDevicePermissions } from "../const/videoChat/types";
import roomModel from "../models/room.model";
import { ApiError } from "../utils/errors/ApiError";

class RoomService {
    async create() {
        const roomId = v4();
        await roomModel.create({
            roomId,
        });

        return { roomId };
    }

    async clearAllUsersInAllRooms() {
        await roomModel.updateMany({}, {
            $set: {
                users: [],
            }
        })
    }

    async setDevicePermissions({ socketId, roomId, devices }: { socketId: string, roomId: string, devices: IDevicePermissions }) {
        await roomModel.findOneAndUpdate({
            roomId,
            "users.socketId": socketId,
        }, {
            $set: { "users.$.devices": devices }
        })
    }

    async join({ userName, socketId, roomId, devices }: { userName: string, socketId: string, roomId: string, devices: IDevicePermissions }) {
        let room = await roomModel.findOne({ roomId });
        if (!room) return {}
        room.users?.push({
            userName,
            socketId,
            devices,
        })
        await room.save()
        const roomDTO = await room.toDto();
        return roomDTO;
    }

    async leave({ socketId, roomId }: { socketId: string, roomId: string }) {
        await roomModel.findOneAndUpdate(
            {
                roomId,
            },
            {
                $pull: {
                    users: {
                        socketId,
                    },
                },
            },
            {
                new: true,
                upsert: true,
            }
        );
    }

    async getRoom(roomId: string) {
        const room = await roomModel.findOne({ roomId });
        if (!room) {
            throw ApiError.BadRequest("Комнаты не существует");
        }
        const roomDTO = await room.toDto();
        return { room: roomDTO };
    }
}

export const roomService = new RoomService();