import {Schema, model} from 'mongoose';

const User = new Schema({
    email: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        unique: false,
        required: true
    },
    isActivated: {
        type: Boolean,
        default: false
    },
    activationLink: {
        type: String
    },
    resetPasswordLink: {
        type: String
    }
});

export interface IUserDto {
    email: string;
    id: string;
    isActivated: boolean;
}

User.methods.toDto = function toDto () {
    return {
        email: this.email,
        id: this._id,
        isActivated: this.isActivated,
    }
}

export default model('User', User);