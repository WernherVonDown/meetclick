import { Server } from 'socket.io';

// import { auth } from '../services/socket/auth';
import { onConnection } from '../utils/socket/onConnection';

const socketConfig = Object.freeze({
    pingInterval: 10000,
    pingTimeout: 10000,
});

const io = new Server(socketConfig);

// io.use(auth);

io.on('connection', onConnection);

export { io };
