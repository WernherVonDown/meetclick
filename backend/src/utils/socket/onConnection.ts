import { Socket } from "socket.io";
import { rootRouter } from "../../socket/root";
import { leave } from "../../socket/root/room/controller";

export const onConnection = (socket: Socket) => {
    console.log(`socket:connected; ${socket.id}`);
    socket.on("disconnect", async (reason)=>{
        console.log(reason);
        await leave(socket, null)
    })
    rootRouter.subscribe(socket);
};
